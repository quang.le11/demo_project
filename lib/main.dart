import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/core/custom_theme.dart';
import 'package:weather_app/features/presentation/home/pages/home_page.dart';
import 'package:weather_app/features/presentation/login/login_page.dart';
import 'package:weather_app/features/presentation/search/blocs/search_bloc.dart';
import 'package:weather_app/features/presentation/search/blocs/search_bloc_observer.dart';
import 'package:weather_app/features/presentation/search/search_page.dart';
import 'package:weather_app/model/theme_model.dart';

import 'features/repository/repository.dart';

void main() {
  final Repository repository = Repository();
  BlocOverrides.runZoned(() => runApp( MyApp(repository: repository,)),
    blocObserver: SearchBlocObserver()
  );
  // runApp( MyApp());
}

class MyApp extends StatelessWidget {
  final Repository repository;
  const MyApp({Key? key,required this.repository }) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ThemeModel(),
      child: Consumer<ThemeModel>(
          builder: (context, ThemeModel themeNotifier, child) {
            return BlocProvider(
                  create: (context) => SearchBloc(repository: repository),
              child: MaterialApp(
                title: 'Flutter Demo',
                theme: themeNotifier.isDark? customDarkTheme() : customLightTheme(),
                debugShowCheckedModeBanner: false,
                home:
                LoginPage()
          //       // HomePage(repository:  repository,)
          //       BlocProvider(
          //       create: (context) => SearchBloc(repository: repository)
          //       ,child:  HomePage(repository: repository,)),
          //     )
          // ,
            ));})
    );
  }
}
