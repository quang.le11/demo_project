//theme_model.dart
import 'package:flutter/material.dart';

import '../core/theme_preferences.dart';

class ThemeModel extends ChangeNotifier {
  late bool _isDark;
  late ThemePreferences _preferences;
  bool get isDark => _isDark;
  List<bool> isSelected =  List.generate(2, (_)=> false);

  ThemeModel() {
    _isDark = false;
    isSelected[0] = true;
    _preferences = ThemePreferences();
    getPreferences();
  }

  set isDark(bool value) {
    _isDark = value;
    _preferences.setTheme(value);
    notifyListeners();
  }

  getPreferences() async {
    _isDark = await _preferences.getTheme();
    notifyListeners();
  }

  onSelected(int index){
    for (int i = 0; i < isSelected.length; i++) {
      isSelected[i] = i == index;
    }
    index != 0 ? isDark = true : isDark = false;

  }
}