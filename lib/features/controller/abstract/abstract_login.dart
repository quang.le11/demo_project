import 'package:weather_app/features/data/model/user.dart';

abstract class AbstractLogin{
  login(User user);
}