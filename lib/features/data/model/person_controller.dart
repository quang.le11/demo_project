import 'package:get/get.dart';
import 'package:weather_app/features/data/model/person.dart';
import 'package:weather_app/features/data/model/user.dart';

class PersonController extends GetxController{
  var person = Person('MinhQuang', 'ThanhMai', '0363215756').obs;

  updatePerson(Person newPerson) => person.value = newPerson;
}