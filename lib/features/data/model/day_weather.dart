class DayWeather {
  DayWeather({
    required this.dt,
    required this.main,
    required this.weather,
    required this.clouds,
    required this.wind,
    required this.visibility,
    required this.pop,
    required this.rain,
    required this.sys,
    required this.dtTxt,
  });
  late final dynamic dt;
  late final Main main;
  late final List<FiveWeather> weather;
  late final Clouds clouds;
  late final Wind wind;
  late final dynamic visibility;
  late final dynamic pop;
  late final Rain rain;
  late final Sys sys;
  late final String dtTxt;

  DayWeather.fromJson(Map<String, dynamic> json){
    var f = json['dt_txt'].split(' ')[0].split('-')[2];
    var l = json['dt_txt'].split(' ')[1].split(':')[0];
    var fandl = '$f-$l';
    main = Main.fromJson(json['main']);
    weather = List.from(json['weather']).map((e)=>FiveWeather.fromJson(e)).toList();
    dtTxt = '$fandl';
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['dt'] = dt;
    _data['main'] = main.toJson();
    _data['weather'] = weather.map((e)=>e.toJson()).toList();
    _data['clouds'] = clouds.toJson();
    _data['wind'] = wind.toJson();
    _data['visibility'] = visibility;
    _data['pop'] = pop;
    _data['rain'] = rain.toJson();
    _data['sys'] = sys.toJson();
    _data['dt_txt'] = dtTxt;
    return _data;
  }
}

class Main {
  Main({
    required this.temp,
    required this.feelsLike,
    required this.tempMin,
    required this.tempMax,
    required this.pressure,
    required this.seaLevel,
    required this.grndLevel,
    required this.humidity,
    required this.tempKf,
  });
  late final dynamic temp;
  late final dynamic feelsLike;
  late final dynamic tempMin;
  late final dynamic tempMax;
  late final dynamic pressure;
  late final dynamic seaLevel;
  late final dynamic grndLevel;
  late final dynamic humidity;
  late final dynamic tempKf;

  Main.fromJson(Map<String, dynamic> json){
    temp = json['temp'];
    feelsLike = json['feels_like'];
    tempMin = json['temp_min'];
    tempMax = json['temp_max'];
    pressure = json['pressure'];
    seaLevel = json['sea_level'];
    grndLevel = json['grnd_level'];
    humidity = json['humidity'];
    tempKf = json['temp_kf'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['temp'] = temp;
    _data['feels_like'] = feelsLike;
    _data['temp_min'] = tempMin;
    _data['temp_max'] = tempMax;
    _data['pressure'] = pressure;
    _data['sea_level'] = seaLevel;
    _data['grnd_level'] = grndLevel;
    _data['humidity'] = humidity;
    _data['temp_kf'] = tempKf;
    return _data;
  }
}

class FiveWeather {
  FiveWeather({
    required this.id,
    required this.main,
    required this.description,
    required this.icon,
  });
  late final dynamic id;
  late final String main;
  late final String description;
  late final String icon;

  FiveWeather.fromJson(Map<String, dynamic> json){
    id = json['id'];
    main = json['main'];
    description = json['description'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['main'] = main;
    _data['description'] = description;
    _data['icon'] = icon;
    return _data;
  }
}

class Clouds {
  Clouds({
    required this.all,
  });
  late final dynamic all;

  Clouds.fromJson(Map<String, dynamic> json){
    all = json['all'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['all'] = all;
    return _data;
  }
}

class Wind {
  Wind({
    required this.speed,
    required this.deg,
    required this.gust,
  });
  late final dynamic speed;
  late final dynamic deg;
  late final dynamic gust;

  Wind.fromJson(Map<String, dynamic> json){
    speed = json['speed'];
    deg = json['deg'];
    gust = json['gust'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['speed'] = speed;
    _data['deg'] = deg;
    _data['gust'] = gust;
    return _data;
  }
}

class Rain {
  Rain({
  required this.threeHours,
});
late final dynamic threeHours;

Rain.fromJson(Map<String, dynamic> json){
  // threeHours = json['3h'];
}

Map<String, dynamic> toJson() {
  final _data = <String, dynamic>{};
  _data['3h'] = threeHours;
  return _data;
}
}

class Sys {
  Sys({
    required this.pod,
  });
  late final String pod;

  Sys.fromJson(Map<String, dynamic> json){
    // pod = json['pod'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['pod'] = pod;
    return _data;
  }
}