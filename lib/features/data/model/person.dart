class Person {
  String name;
  String address;
  String phoneNumber;

  Person(this.name, this.address, this.phoneNumber);

  @override
  String toString() {
    return '$name\n$address\n$phoneNumber';
  }
}