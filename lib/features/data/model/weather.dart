import 'package:equatable/equatable.dart';

class Weather extends Equatable{
  Weather({
     this.coord,
     this.weatherDetail,
     this.base,
     this.mainAttribute,
     this.visibility,
     this.wind,
     this.clouds,
     this.dt,
     this.sys,
     this.timezone,
     this.id,
     this.name,
     this.cod,
  });
  late final Coord? coord;
  late final List<WeatherDetail>? weatherDetail;
  late final String? base;
  late final MainAttribute? mainAttribute;
  late final dynamic? visibility;
  late final Wind? wind;
  late final Clouds? clouds;
  late final dynamic? dt;
  late final Sys? sys;
  late final dynamic? timezone;
  late final dynamic? id;
  late final String? name;
  late final dynamic? cod;

  Weather.fromJson(Map<String, dynamic> json){
    coord = Coord.fromJson(json['coord']);
    weatherDetail = List.from(json['weather']).map((e)=>WeatherDetail.fromJson(e)).toList();
    base = json['base'];
    mainAttribute = MainAttribute.fromJson(json['main']);
    visibility = json['visibility'];
    wind = Wind.fromJson(json['wind']);
    clouds = Clouds.fromJson(json['clouds']);
    dt = json['dt'];
    sys = Sys.fromJson(json['sys']);
    timezone = json['timezone'];
    id = json['id'];
    name = json['name'];
    cod = json['cod'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['coord'] = coord?.toJson();
    _data['weather'] = weatherDetail?.map((e)=>e.toJson()).toList();
    _data['base'] = base;
    _data['main'] = mainAttribute?.toJson();
    _data['visibility'] = visibility;
    _data['wind'] = wind?.toJson();
    _data['clouds'] = clouds?.toJson();
    _data['dt'] = dt;
    _data['sys'] = sys?.toJson();
    _data['timezone'] = timezone;
    _data['id'] = id;
    _data['name'] = name;
    _data['cod'] = cod;
    return _data;
  }

  @override
  List<Object> get props => [];
}

class Coord {
  Coord({
    required this.lon,
    required this.lat,
  });
  late final dynamic lon;
  late final dynamic lat;

  Coord.fromJson(Map<String, dynamic> json){
    lon = json['lon'];
    lat = json['lat'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['lon'] = lon;
    _data['lat'] = lat;
    return _data;
  }
}

class WeatherDetail {
  WeatherDetail({
    required this.id,
    required this.main,
    required this.description,
    required this.icon,
  });
  late final dynamic id;
  late final String main;
  late final String description;
  late final String icon;

  WeatherDetail.fromJson(Map<String, dynamic> json){
    id = json['id'];
    main = json['main'];
    description = json['description'];
    icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['main'] = main;
    _data['description'] = description;
    _data['icon'] = icon;
    return _data;
  }
}

class MainAttribute {
  MainAttribute({
    required this.temp,
    required this.feelsLike,
    required this.tempMin,
    required this.tempMax,
    required this.pressure,
    required this.humidity,
  });
  late final dynamic temp;
  late final dynamic feelsLike;
  late final dynamic tempMin;
  late final dynamic tempMax;
  late final dynamic pressure;
  late final dynamic humidity;

  MainAttribute.fromJson(Map<String, dynamic> json){
    temp = json['temp'];
    feelsLike = json['feels_like'];
    tempMin = json['temp_min'];
    tempMax = json['temp_max'];
    pressure = json['pressure'];
    humidity = json['humidity'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['temp'] = temp;
    _data['feels_like'] = feelsLike;
    _data['temp_min'] = tempMin;
    _data['temp_max'] = tempMax;
    _data['pressure'] = pressure;
    _data['humidity'] = humidity;
    return _data;
  }
}

class Wind {
  Wind({
    required this.speed,
    required this.deg,
    required this.gust,
  });
  late final dynamic speed;
  late final dynamic deg;
  late final dynamic gust;

  Wind.fromJson(Map<String, dynamic> json){
    speed = json['speed'];
    deg = json['deg'];
    gust = json['gust'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['speed'] = speed;
    _data['deg'] = deg;
    _data['gust'] = gust;
    return _data;
  }

  @override
  String toString() {
    return 'Wind{speed: $speed, deg: $deg, gust: $gust}';
  }
}

class Clouds {
  Clouds({
    required this.all,
  });
  late final dynamic all;

  Clouds.fromJson(Map<String, dynamic> json){
    all = json['all'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['all'] = all;
    return _data;
  }
}

class Sys {
  Sys({
    required this.type,
    required this.id,
    required this.country,
    required this.sunrise,
    required this.sunset,
  });
  late final dynamic type;
  late final dynamic id;
  late final String country;
  late final dynamic sunrise;
  late final dynamic sunset;

  Sys.fromJson(Map<String, dynamic> json){
    type = json['type'];
    id = json['id'];
    country = json['country'];
    sunrise = json['sunrise'];
    sunset = json['sunset'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['type'] = type;
    _data['id'] = id;
    _data['country'] = country;
    _data['sunrise'] = sunrise;
    _data['sunset'] = sunset;
    return _data;
  }
}