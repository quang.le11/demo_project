import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/core/extension_valid.dart';
import 'package:weather_app/features/presentation/sign_up/sign_up_controller.dart';
import 'package:weather_app/features/presentation/widgets/common_button.dart';
import 'package:weather_app/features/presentation/widgets/custom_text_field.dart';


class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  late FocusNode myFocusNode;
  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          backgroundColor: Theme.of(context).backgroundColor,
          title: Text(
            'Sign Up',
            style: Theme.of(context)
                .textTheme
                .headline1
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        body: GetBuilder<SignUpController>(
          init: SignUpController(),
          builder: (controller) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  CustomTextFormField(
                    // focusNode: myFocusNode,
                    onChanged: (val){
                      if(val.isNotEmpty){
                        controller.isFill();
                      }
                    },
                    controller: controller.nameController,
                    autoFocus: true,
                    hintText: 'Name',
                    validator: (val) {
                      if (val!.isEmpty) {
                        // Focus.of(context).requestFocus(myFocusNode);
                        return 'Name cannot be empty';
                      }
                      if (!val.toString().isValidName) {
                        // Focus.of(context).requestFocus(myFocusNode);
                        return 'Invalid Name';
                      }
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    // focusNode: myFocusNode,
                    // focusNode: myFocusNode.hasFocus ?  myFocusNode : empty,
                    onChanged: (val){
                      if(val.isNotEmpty){
                        controller.isFill();
                      }
                    },
                    controller: controller.emailController,

                    hintText: 'Email',
                    validator: (val) {
                      if (val!.isEmpty) {
                        // Focus.of(context).requestFocus(myFocusNode);
                        return 'Email cannot be empty';
                      }
                      if (!val.toString().isValidEmail) {
                        // Focus.of(context).requestFocus(myFocusNode);

                        return 'Invalid Email Format';
                      }
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    // focusNode: myFocusNode,
                    onChanged: (val){
                      if(val.isNotEmpty){
                        controller.isFill();
                      }
                    },
                    controller: controller.phoneController,

                    hintText: 'Phone Number',
                    validator: (val) {
                      if (val!.isEmpty) {
                        // myFocusNode.hasFocus ? print('nnooo') :
                        // Focus.of(context).requestFocus(myFocusNode);

                        return 'Phone Number cannot be empty';
                      }
                      if (!val.toString().isValidPhone) {
                        // Focus.of(context).requestFocus(myFocusNode);

                        return 'Invalid Phone Number';
                      }
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    // focusNode: myFocusNode,
                    onChanged: (val){
                      if(val.isNotEmpty){
                        controller.isFill();
                      }
                    },
                    controller: controller.passwordController,
                    hintText: 'Password',
                    validator: (val) {
                      if (val!.isEmpty) {
                        // Focus.of(context).requestFocus(myFocusNode);
                        // Focus.of(context).hasFocus;
                        return 'Password cannot be empty';
                      }
                      if (!val.toString().isValidPassword) {
                        return 'Invalid Password';
                      }
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    maxLines: 5,
                    hintText: 'Description',
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: CommonButton(title: 'SignUp', onPressed:  !controller.enableButton ? null : () => {
                          if (_formKey.currentState!.validate()){}
                        },)),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            );
          }
        ),
      ),
    );
  }
}
