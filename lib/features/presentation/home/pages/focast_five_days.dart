import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../data/model/day_weather.dart';

class NextFiveDays extends StatelessWidget {
  final List<DayWeather> fiveDays;
   NextFiveDays({Key? key, required this.fiveDays}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: MediaQuery.of(context).size.width,
      height: 240,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: SfCartesianChart(
          primaryXAxis: CategoryAxis(),
          series: <ChartSeries<DayWeather, String>>[
            SplineSeries<DayWeather, String>(
              dataSource: fiveDays,
              xValueMapper: (DayWeather f, _) =>
              f.dtTxt,
              yValueMapper: (DayWeather f, _) =>
              f.main.temp - 273.15,
            ),
          ],
        ),
      ),
    );
  }
}
