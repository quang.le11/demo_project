import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:weather_app/features/data/model/day_weather.dart';
import 'package:weather_app/features/data/model/person_controller.dart';
import 'package:weather_app/features/data/model/weather.dart';
import 'package:weather_app/features/presentation/home/controller/home_controller.dart';
import 'package:weather_app/features/presentation/home/pages/focast_five_days.dart';
import 'package:weather_app/features/presentation/home/pages/other_city_screen.dart';
import 'package:weather_app/features/presentation/search/search_page.dart';
import 'package:weather_app/features/presentation/settings/settings_page.dart';

import '../../../../model/theme_model.dart';
import '../../../repository/repository.dart';
import '../../person/info_screen.dart';
import '../../search/blocs/search_bloc.dart';

class HomePage extends StatefulWidget {
  final Repository repository;
  const HomePage({Key? key, required this.repository}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PersonController personController = Get.put(PersonController());

  @override
  Widget build(BuildContext context) {
    // final themeNotifier = context.watch<ThemeModel>();
    return Consumer<ThemeModel>(
        builder: (context, ThemeModel themeNotifier, child) {
      return Scaffold(
        // extendBodyBehindAppBar: true,
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const SettingPage()));
              },
              icon: Icon(
                Icons.settings,
                color: Theme.of(context).primaryColor,
              )),
          actions: [
            BlocProvider(
              create: (context) => SearchBloc(repository: widget.repository),
              child: IconButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => const SearchPage()));
                  },
                  icon: Icon(
                    Icons.search,
                    color: Theme.of(context).primaryColor,
                  )),
            ),
            IconButton(onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => PersonScreen()));
            }, icon: Icon(
              Icons.person,
              color: Theme.of(context).primaryColor,
            ))
          ],
          backgroundColor: Theme.of(context).backgroundColor,
          title: Obx(() =>Text(
            'Hello ${personController.person.value.name.toUpperCase()}',
            style: Theme.of(context)
                .textTheme
                .headline1
                ?.copyWith(fontWeight: FontWeight.bold),
          ),),
          centerTitle: true,
        ),
        body: GetBuilder<HomeController>(
            init: HomeController(city: 'HaNoi'),
            builder: (controller) {
              return controller.isLoading
                  ? SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        // mainAxisAlignmentnment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 0),
                            child: Wrap(
                              // width: double.infinity,
                              // height: MediaQuery.of(context).size.height / 3.5,
                              children: [
                                Card(
                                  color: Theme.of(context).cardColor,
                                  elevation: 5,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 15.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Center(
                                            child: Text(
                                          controller.curretWeather.name
                                                  ?.toUpperCase() ??
                                              'No Data',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline2,
                                        )),
                                        Center(
                                            child: Text(
                                          DateFormat()
                                              .add_MMMMEEEEd()
                                              .format(DateTime.now()),
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        )),
                                        const Divider(),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              padding:
                                                  const EdgeInsets.only(left: 50),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    'Current Weather',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline3,
                                                  ),
                                                  Text(
                                                    '${(controller.curretWeather.mainAttribute?.temp - 273).round().toString()}\u2103',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline6,
                                                  ),
                                                  Text(
                                                    'min: ${(controller.curretWeather.mainAttribute?.tempMin - 273).round().toString()}\u2103 / max :${(controller.curretWeather.mainAttribute?.tempMax - 273).round().toString()}\u2103',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .headline4,
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: const
                                                  EdgeInsets.only(right: 40),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Container(
                                                    width: 100,
                                                    height: 100,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: AssetImage(themeNotifier
                                                                .isDark
                                                            ? 'assets/images/moonlight.png'
                                                            : 'assets/images/icon-01.jpeg'),
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                  Text('wind: 10 m/s',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .caption),
                                                ],
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),

                          // Center(
                          //   child: SizedBox(
                          //     height: 50,
                          //     width: MediaQuery.of(context).size.width/2,
                          //     child: ElevatedButton(
                          //       child: Text('Other City'),
                          //       onPressed: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => OtherCity()))
                          //     ),
                          //   ),
                          // ),
                          Padding(
                            padding:const EdgeInsets.only(left: 10),
                            child: Text(
                              'Other City',
                              style: Theme.of(context).textTheme.caption,
                            ),
                          ),
                          SizedBox(
                              height: 150,
                              child: OtherCity(
                                dataList: controller.dataList,
                              )),
                          //Focast Next 5 days
                          Container(
                            padding:const  EdgeInsets.only(top: 10, left: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('forcast next 5 days'.toUpperCase(),
                                    style: Theme.of(context).textTheme.caption),
                                Icon(
                                  Icons.next_plan_outlined,
                                  color: Theme.of(context).iconTheme.color,
                                ),
                              ],
                            ),
                          ),
                          NextFiveDays(fiveDays: controller.fiveDays),
                          const SizedBox(
                            height: 15,
                          )
                        ],
                      ),
                    )
                  : const  Center(child: CircularProgressIndicator());
            }),
      );
    });
  }
}
