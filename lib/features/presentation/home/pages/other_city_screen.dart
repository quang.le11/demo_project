import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../model/theme_model.dart';
import '../../../data/model/weather.dart';

class OtherCity extends StatelessWidget {
  List<Weather> dataList;
  OtherCity({Key? key,required this.dataList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   Consumer<ThemeModel>(
        builder: (context, ThemeModel themeNotifier, child) {
        return ListView.separated(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          separatorBuilder: (context, index) =>
              VerticalDivider(
                color: Colors.transparent,
                width: 5,
              ),
          itemCount: dataList.length,
          itemBuilder: (context, index) {
            Weather? data;
            (dataList.length > 0)
                ? data = dataList[index]
                : data = null;
            return Container(
              padding: EdgeInsets.only(left: 10),
              width: 140,
              height: 150,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Container(
                  child: Column(
                    mainAxisAlignment:
                    MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        (data != null)
                            ? '${data.name}'
                            : '',
                        style: Theme.of(context)
                            .textTheme
                            .caption
                            ?.copyWith(
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        (data != null)
                            ? '${(data.mainAttribute?.temp - 273.15).round().toString()}\u2103'
                            : '',
                        style: Theme.of(context)
                            .textTheme
                            .caption
                      ),
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image:  AssetImage(
                                themeNotifier.isDark ? 'assets/images/moonlight.png' :
                                'assets/images/icon-01.jpeg'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Text(
                        (data != null)
                            ? '${data.weatherDetail?.first.description}'
                            : '',
                        style: Theme.of(context)
                            .textTheme
                            .caption
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      }
    );;
  }
}

