import 'dart:convert';

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:weather_app/features/data/model/weather.dart';

import '../../../data/model/day_weather.dart';
import 'package:http/http.dart' as http;

const baseUrl = 'https://api.openweathermap.org/data/2.5';
const apiKey = 'e9547fa0b43e2e2dd1899cd429450185';

class HomeController extends GetxController {
  String city;
  Weather curretWeather = Weather();
  List<Weather> dataList = [];
  List<DayWeather> fiveDays = [];
  HomeController({required this.city});
  var client = http.Client();
  bool isLoading = false;

  @override
  void onInit() {
    // TODO: implement onInit
    initState();
    super.onInit();
  }

  updateWeather() {
    initState();
  }

  initState() async {
    await getCurrentWeatherData();
    await getTopFiveCities();
    await getFiveDaysData();
    isLoading = true;
    update();
  }

  getCurrentWeatherData() async {
    var response =
        await client.get(Uri.parse('$baseUrl/weather?q=$city&appid=$apiKey'));
    if (response.statusCode == 200) {
      var weather = response.body;
      curretWeather = Weather.fromJson(jsonDecode(weather));
      // update();
    }
  }

  getTopFiveCities() {
    List<String> cities = ['London', 'New York', 'Paris', 'Moscow', 'Tokyo'];
    cities.forEach((c) async {
      var response =
          await client.get(Uri.parse('$baseUrl/weather?q=$city&appid=$apiKey'));
      if (response.statusCode == 200) {
        var weather = response.body;
        dataList.add(Weather.fromJson(jsonDecode(weather)));
        // update();
      }
    });
  }

  getFiveDaysData() async {
    var response =
        await client.get(Uri.parse('$baseUrl/forecast?q=$city&appid=$apiKey'));
    if (response.statusCode == 200) {
      var weather = jsonDecode(response.body)['list'] as List;
      fiveDays = weather.map((e) => DayWeather.fromJson(e)).toList();
      // update();
    }
  }
}
