import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/features/controller/controller/login.dart';
import 'package:weather_app/features/data/model/user.dart';
import 'package:weather_app/features/presentation/home/pages/home_page.dart';
import 'package:weather_app/features/presentation/sign_up/sign_up_screen.dart';

import '../../repository/repository.dart';

class LoginController extends GetxController {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String text = 'dasdas';
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  onLogin(BuildContext context) {
    User user = User('', '');
    Login login = Login();
    String username = usernameController.text;
    String password = passwordController.text;
      login.login(user);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(repository: Repository())),
      );
  }

  onSignUp(BuildContext context){
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SignUpScreen()),
    );
  }
}
