import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:weather_app/core/extension_valid.dart';
import 'package:weather_app/features/presentation/widgets/custom_text_field.dart';
import '../widgets/common_button.dart';
import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  late FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: GetBuilder<LoginController>(
              init: LoginController(),
              builder: (controller) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Login',
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          ?.copyWith(fontSize: 35),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    CustomTextFormField(
                        autoFocus: true,
                        // focusNode: myFocusNode,
                        validator: (val){
                          if(val!.isEmpty){
                            return 'Username cannot be empty';
                          }
                          if(!val.toString().isValidName  ){
                            return 'Invalid username';
                          }
                        },
                        controller: controller.usernameController,
                        hintText: 'Username'),
                    const SizedBox(
                      height: 10,
                    ),
                    CustomTextFormField(
                        focusNode: myFocusNode,
                        obscure: true,
                        validator: (val){
                          if(val!.isEmpty){
                            return 'Password cannot be empty';
                          }
                          if(!val.toString().isValidPassword  ){
                            return 'Invalid Password';
                          }
                        },
                        controller: controller.passwordController,
                        hintText: 'Password'),
                    const SizedBox(
                      height: 50,
                    ),
                    CommonButton(title: 'Login', onPressed: (){
                      if (_formKey.currentState!.validate()) {
                        controller.onLogin(context);

                      }
                    }),
                    const SizedBox(
                      height: 20,
                    ),
                    CommonButton(title: 'SignUp',
                      onPressed: () => controller.onSignUp(context),)
                  ],
                );
              }),
        ),
      ),
    );
  }
}
