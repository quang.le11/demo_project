import 'dart:ffi';

import 'package:flutter/material.dart';

class CustomTextFormField extends StatefulWidget {
  TextEditingController? controller;
  String? hintText;
  int? maxLines;
  bool? autoFocus;
  FocusNode? focusNode;
  Function(String)? onChanged;
  bool? obscure;
  final String? Function(String?)? validator;
  CustomTextFormField({Key? key,this.obscure, this.onChanged, this.focusNode, this.autoFocus, this.controller,  this.hintText, this.maxLines, this.validator}) : super(key: key);

  @override
  State<CustomTextFormField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: widget.obscure ?? false,
      onChanged: widget.onChanged,
      autofocus: widget.autoFocus ?? false,
      focusNode: widget.focusNode,
      validator: widget.validator,
      controller: widget.controller,
      maxLines: widget.maxLines ?? 1,
      decoration: InputDecoration(
        hintText: widget.hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
            color: Theme.of(context).primaryColor
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
              color: Theme.of(context).focusColor
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
              color: Theme.of(context).errorColor
          ),
        )
      ),
    );
  }
}
