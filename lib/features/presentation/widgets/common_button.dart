import 'package:flutter/material.dart';

class CommonButton extends StatefulWidget {
  final String title;
  final VoidCallback? onPressed;
  const CommonButton({Key? key, required this.title, required this.onPressed}) : super(key: key);

  @override
  State<CommonButton> createState() => _CommonButtonState();
}

class _CommonButtonState extends State<CommonButton> {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:const BoxConstraints.expand(width: double.infinity, height: 50),
      child: ElevatedButton(
          onPressed:  widget.onPressed,
          child:  Text(widget.title)),
    );
  }
}
