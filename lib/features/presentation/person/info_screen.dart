import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/features/data/model/person_controller.dart';
import 'package:weather_app/features/presentation/person/update_infor/update_infor_screen.dart';
import 'package:weather_app/features/presentation/widgets/common_button.dart';

class PersonScreen extends StatelessWidget {
  final PersonController _person = Get.put(PersonController());
   PersonScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).backgroundColor,
        leading: IconButton(
          icon:  Icon(
            Icons.arrow_back_ios_new,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: ()=> Navigator.pop(context),
        ),
        centerTitle: true,
        title: Text('Information', style: Theme.of(context).textTheme.headline1,),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children:  [
            SizedBox(height: 30,),
            Obx(() => Text(_person.person.value.name,style: Theme.of(context).textTheme.headline2,),
            ),
            SizedBox(height: 10,),
            Obx(() => Text(_person.person.value.address,style: Theme.of(context).textTheme.headline2,),
            ),
            SizedBox(height: 10,),
            Obx(() => Text(_person.person.value.phoneNumber,style: Theme.of(context).textTheme.headline2,),
            ),
            SizedBox(height: 10,),
            Expanded(child: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
                child: CommonButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateInforScreen()));
                  },
                  title: 'Update',
                ),
              ),
            ))
          ],
        ),
      ),
    );
  }
}
