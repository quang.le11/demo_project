import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:weather_app/core/extension_valid.dart';
import 'package:weather_app/features/data/model/day_weather.dart';
import 'package:weather_app/features/presentation/login/login_controller.dart';
import 'package:weather_app/features/presentation/person/update_infor/update_infor_controller.dart';
import 'package:weather_app/features/presentation/widgets/custom_text_field.dart';

import '../../widgets/common_button.dart';

class UpdateInforScreen extends StatefulWidget {
  const UpdateInforScreen({Key? key}) : super(key: key);

  @override
  State<UpdateInforScreen> createState() => _UpdateInforScreenState();
}

class _UpdateInforScreenState extends State<UpdateInforScreen> {
  final _formKey = GlobalKey<FormState>();
  late FocusNode myFocusNode;
  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Theme.of(context).backgroundColor,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios_new,
                color: Theme.of(context).primaryColor,
              ),
              onPressed: () => Navigator.pop(context),
            ),
            centerTitle: true,
            title: Text(
              'Information',
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          body: GetBuilder<UpdateInfoController>(
              init: UpdateInfoController(),
              builder: (controller) {
                return Stack(
                  children: [
                    SingleChildScrollView(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                onChanged: (val) {
                                  if (val.isNotEmpty) {
                                    controller.isFill();
                                  }
                                },
                                controller: controller.nameController,
                                autoFocus: true,
                                hintText: 'Name',
                                validator: (val) {
                                  if (val!.isEmpty) {
                                    // Focus.of(context).requestFocus(myFocusNode);
                                    return 'Name cannot be empty';
                                  }
                                  if (!val.toString().isValidName) {
                                    // Focus.of(context).requestFocus(myFocusNode);
                                    return 'Invalid Name';
                                  }
                                },
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                onChanged: (val) {
                                  if (val.isNotEmpty) {
                                    controller.isFill();
                                  }
                                },
                                controller: controller.addressController,
                                autoFocus: true,
                                hintText: 'Address',
                                validator: (val) {
                                  if (val!.isEmpty) {
                                    // Focus.of(context).requestFocus(myFocusNode);
                                    return 'Address cannot be empty';
                                  }
                                  if (!val.toString().isValidName) {
                                    // Focus.of(context).requestFocus(myFocusNode);
                                    return 'Address Name';
                                  }
                                },
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                onChanged: (val) {
                                  if (val.isNotEmpty) {
                                    controller.isFill();
                                  }
                                },
                                controller: controller.phoneController,
                                autoFocus: true,
                                hintText: 'Phone Number',
                                validator: (val) {
                                  if (val!.isEmpty) {
                                    // Focus.of(context).requestFocus(myFocusNode);
                                    return 'Phone Number cannot be empty';
                                  }
                                  if (!val.toString().isValidPhone) {
                                    // Focus.of(context).requestFocus(myFocusNode);
                                    return 'Invalid Phone Number';
                                  }
                                },
                              ),
                            ],
                          )),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 8),
                          child: CommonButton(
                            title: 'Save',
                            onPressed: !controller.enableButton
                                ? null
                                : () => {
                                      if (_formKey.currentState!.validate())
                                        {
                                          controller.onUpdateInformation(),
                                          showTopSnackBar(
                                              context,
                                              const CustomSnackBar.success(
                                                  message: 'Success')),
                                        }
                                    },
                          )),
                    )
                  ],
                );
              }),
        ));
  }
}
