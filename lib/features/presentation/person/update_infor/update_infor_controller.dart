import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:weather_app/features/data/model/person.dart';

import '../../../data/model/person_controller.dart';

class UpdateInfoController extends GetxController{
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  bool enableButton = false;

  final PersonController _person = Get.put(PersonController());

  @override
  void onInit() {
    // TODO: implement onInit
    initState();
    super.onInit();
  }

  initState(){
    nameController.text = _person.person.value.name;
    addressController.text = _person.person.value.address;
    phoneController.text = _person.person.value.phoneNumber;
  }

  onUpdateInformation(){
    Person person = Person(nameController.text,addressController.text,phoneController.text);
    _person.updatePerson(person);
  }

  isFill(){
    if((nameController.text.isNotEmpty || nameController.text != '')
        && (phoneController.text.isNotEmpty || phoneController.text != '')
        && (addressController.text.isNotEmpty || addressController.text != '')){
      enableButton = true;
      update();
    }else{
      enableButton = false;
      update();
    }
  }
}