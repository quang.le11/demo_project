import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../model/theme_model.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {


  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeModel>(
        builder: (context, ThemeModel themeNotifier, child){
        return Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            leading: IconButton(
              onPressed: (){
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios_new, color: Theme.of(context).primaryColor,),
            ),
            title: Text('Settings', style: Theme.of(context).textTheme.headline1,),
            centerTitle: true,
          ),
          body: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 10,top: 10),
                child: Text('Dark Mode', style: Theme.of(context)
                    .textTheme
                    .headline2,),
              ),
              Padding(padding: EdgeInsets.only(right: 10,top: 10),
                child: ToggleButtons(
                  // borderColor: Colors.black,
                  fillColor: Colors.grey,
                  borderWidth: 2,
                  // selectedBorderColor: Colors.black,
                  selectedColor: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        'Light',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        'Dark',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                  onPressed: (int index) => themeNotifier.onSelected(index),
                  isSelected: themeNotifier.isSelected,
                ),
              )
            ],
          ),
        );
      }
    );
  }
}
