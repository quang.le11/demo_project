import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:weather_app/features/presentation/search/blocs/search_bloc.dart';
import 'package:weather_app/features/presentation/search/blocs/search_events.dart';
import 'package:weather_app/features/presentation/search/blocs/search_state.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController searchControler = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        leading: IconButton(
          onPressed: (){
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios_new, color: Theme.of(context).primaryColor,),
        ),
        title: Text('Search', style: Theme.of(context).textTheme.headline1,),
        centerTitle: true,
      ),
      body: Column(
        children: [
        Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        child:   Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 20),
          child: TextField(
            // onChanged: (value)=> {
            //   searchControler.text = value
            // },
            onSubmitted: (value) => context.read<SearchBloc>().add(SearchEventsRequest(city: searchControler.text)),
            controller: searchControler,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              hintText: 'Search',
              suffixIcon: Icon(
                Icons.search,
                color: Colors.black,
              ),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(10)),
            ),
          ),
        ),),
          BlocBuilder<SearchBloc, SearchState>(
            builder: (context, searchState){
              if(searchState is SearchStateLoading){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if(searchState is SearchStateSuccess){
                final weather = searchState.weather;
                return Center(
                  child: Column(
                    children: [
                      Text(weather.name ?? ' No Data', style: Theme.of(context).textTheme.headline2,),
                      SizedBox(height: 10,),
                      Text('Temp: ${(weather.mainAttribute?.temp ?? 0 - 273).round().toString()}\u2103' , style: Theme.of(context).textTheme.headline2,),
                      SizedBox(height: 10,),
                      Text('Temp min: ${(weather.mainAttribute?.tempMin ?? 0 - 273).round().toString()}\u2103', style: Theme.of(context).textTheme.headline2,),
                      SizedBox(height: 10,),
                      Text('Temp max: ${(weather.mainAttribute?.tempMax ?? 0 - 273).round().toString()}\u2103', style: Theme.of(context).textTheme.headline2,),
                      SizedBox(height: 10,),
                      Text('Wind speed: ${weather.wind?.speed.toString() ?? ''}' , style: Theme.of(context).textTheme.headline2,),
                    ],
                  ),
                );
              }
              if(searchState is SearchStateFailure){
                return Center(
                  child: Text('No Data', style: Theme.of(context).textTheme.headline2,),
                );
              }
              return SizedBox();
    },
          ),
        ],
      ),
    );
  }
}
