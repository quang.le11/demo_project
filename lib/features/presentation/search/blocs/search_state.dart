 import 'package:equatable/equatable.dart';
import 'package:weather_app/features/data/model/day_weather.dart';
import 'package:weather_app/features/data/model/weather.dart';

abstract class SearchState extends Equatable{
  const SearchState();

  @override
  List<Object> get props  => [];
}

class SearchStateInitial extends SearchState{}
class SearchStateLoading extends SearchState{}
class SearchStateSuccess extends SearchState{
  final Weather weather;

  SearchStateSuccess({required this.weather});

  @override
  List<Object> get props  => [weather];
}

class SearchStateFailure extends SearchState{}
