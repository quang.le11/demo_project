import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/features/presentation/search/blocs/search_events.dart';
import 'package:weather_app/features/presentation/search/blocs/search_state.dart';

import '../../../data/model/weather.dart';
import '../../../repository/repository.dart';

class SearchBloc extends Bloc<SearchEvents, SearchState>{
  final Repository repository;

  SearchBloc({required this.repository}) : super(SearchStateInitial()){
    on<SearchEventsRequest>( _onSearch);
    on<SearchEventsRefresh>( _onRefresh);
  }

  _onSearch(SearchEventsRequest event, Emitter<SearchState> emit)async{
    emit(SearchStateLoading());
      try{
        Weather curretWeather = await repository.getCurrentWeatherData(event.city);
        if(curretWeather.name == '') emit(SearchStateFailure());
        emit(SearchStateSuccess(weather: curretWeather));
      }catch(exception){
        emit( SearchStateFailure());
      }
  }

  _onRefresh(SearchEventsRefresh event, Emitter<SearchState> emit)async{
    emit(SearchStateLoading());
    try{
      Weather curretWeather = await repository.getCurrentWeatherData(event.city);
      if(curretWeather.name?.isEmpty ?? false) emit(SearchStateFailure());
      emit(SearchStateSuccess(weather: curretWeather));
    }catch(exception){
      emit( SearchStateFailure());
    }
  }
}