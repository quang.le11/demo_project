import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class SearchEvents extends Equatable{
  const SearchEvents();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SearchEventsRequest extends SearchEvents{
  final String city;
  const SearchEventsRequest({required this.city}) : assert(city != null);
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SearchEventsRefresh extends SearchEvents{
  final String city;
  const SearchEventsRefresh({required this.city}) : assert(city != null);
  @override
  // TODO: implement props
  List<Object?> get props => [];
}