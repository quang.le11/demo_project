import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:weather_app/features/data/model/weather.dart';

const baseUrl = 'https://api.openweathermap.org/data/2.5';
const apiKey = 'e9547fa0b43e2e2dd1899cd429450185';

class Repository{
  var client = http.Client();

  Future<Weather> getCurrentWeatherData(String city)  async{
     Weather currentWeather = Weather(name: 'w');
    var response = await client.get(Uri.parse('$baseUrl/weather?q=$city&appid=$apiKey'));
    if(response.statusCode == 200){
      var weather = response.body;
        currentWeather = Weather.fromJson(jsonDecode(weather));
    }
     return currentWeather;

  }
}