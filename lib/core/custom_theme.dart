import 'package:flutter/material.dart';


ThemeData customLightTheme() {

  TextTheme _customLightThemesTextTheme(TextTheme base) {
    return base.copyWith(
      headline1: base.headline1?.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 23.0,
        color: Colors.black,
      ),
      headline6: base.headline6?.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 40.0,
        color: Colors.black,
      ),
      headline4: base.headline6?.copyWith(
        fontSize: 12.0,
        color: Colors.black,
      ),
      headline3: base.headline1?.copyWith(
          fontSize: 16.0,
          color: Colors.black,
      ),
      headline2: base.headline1?.copyWith(
          fontSize: 18.0,
          color: Colors.black,
        fontWeight: FontWeight.bold
      ),
      caption: base.caption?.copyWith(
        color: Colors.black45,
        fontSize: 14,
        fontWeight:
        FontWeight.bold,
        fontFamily:
        'flutterfonts',
      ),
      bodyText2: base.bodyText2?.copyWith(color: Color(0xFF807A6B)),
      bodyText1: base.bodyText1?.copyWith(color: Colors.brown),
    );
  }

  final ThemeData lightTheme = ThemeData.light();
  return lightTheme.copyWith(
    textTheme: _customLightThemesTextTheme(lightTheme.textTheme),
    primaryColor: Colors.blue,
    scaffoldBackgroundColor: Color(0xFFF5F5F5),
    iconTheme: lightTheme.iconTheme.copyWith(
      color: Colors.black45,
    ),
    buttonTheme: ButtonThemeData(
        buttonColor: Colors.blue,
        disabledColor: Colors.grey
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
            textStyle: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold
            ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          )
        ),
    ),
    focusColor: Colors.indigo,
    buttonColor: Colors.white,
    backgroundColor: Color(0xfff3f9fc),
    cardColor: Colors.white,
    errorColor: Colors.red,

  );
}


ThemeData customDarkTheme() {
  TextTheme _customDarkTextTheme(TextTheme base){
    return base.copyWith(
      headline1: base.headline1?.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 23.0,
        color: Colors.white,
      ),
      headline6: base.headline6?.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 40.0,
        color: Colors.white,
      ),
      headline4: base.headline6?.copyWith(
        fontSize: 12.0,
        color: Colors.white,
      ),
      headline3: base.headline1?.copyWith(
        fontSize: 16.0,
        color:Colors.white,
      ),
      headline2: base.headline1?.copyWith(
          fontSize: 18.0,
          color: Colors.white,
          fontWeight: FontWeight.bold
      ),
      caption: base.caption?.copyWith(
        color:Colors.white,
        fontSize: 14,
        fontWeight:
        FontWeight.bold,
        fontFamily:
        'flutterfonts',
      ),
      bodyText2: base.bodyText2?.copyWith(color: Color(0xFF807A6B)),
      bodyText1: base.bodyText1?.copyWith(color: Colors.brown),
    );
  }

  final ThemeData darkTheme = ThemeData.dark();
  return darkTheme.copyWith(
    primaryColor: Colors.white70,
      focusColor: Colors.white,
      cardColor: Colors.black,
      indicatorColor: Color(0xFF807A6B),
    accentColor: Color(0xFFFFF8E1),
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blue,
      disabledColor: Colors.grey
    ),
    primaryIconTheme: darkTheme.primaryIconTheme.copyWith(
      color: Colors.green,
      size: 20,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(

    ),
    textTheme: _customDarkTextTheme(darkTheme.textTheme),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: Colors.grey,
          textStyle: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
          )
      )
    )
  );
}